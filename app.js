var express = require('express');
var app = express();
var randomString = require('random-string');
var bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({ extended: true }); 

var tokens = [];

app.post('/login', urlencodedParser, function(req, res) {
	
	if (req.body.login == 'test' && req.body.password == 'test123') {
		var token = randomString({length : 16});
		tokens.push(token);
		
		res.status(403).send(JSON.stringify({ 'token' : token }));
		return;
	}
	
	res.send(JSON.stringify({ 'error' : 'Invalid login or password' }));
});

app.get('/motd', function(req, res) {
	
	var token = req.query.token || req.headers.token;
	if (!token || tokens.indexOf(token) == -1) {
		res.status(403).send(JSON.stringify({ 'error' : 'Invalid token' }));
		return;
	}
	
	var messages = [
		(new Buffer("Hello World").toString('base64')),
		(new Buffer("Hello iOS").toString('base64')),
		(new Buffer("Hello Objective-C").toString('base64')),
		(new Buffer("Hello Swift?").toString('base64'))
	];
	res.send(JSON.stringify({ 'messages' : messages }));
});

var server = app.listen(1337, function() {
	var host = server.address().address;
	var port = server.address().port;
	
	console.log('Example app listening at http://%s:%s', host, port);
});